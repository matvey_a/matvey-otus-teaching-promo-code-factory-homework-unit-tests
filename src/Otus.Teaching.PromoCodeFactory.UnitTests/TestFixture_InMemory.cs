﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public class TestFixture_InMemory : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }

        private IServiceCollection ServiceCollection { get; set; }

        /// <summary>
        /// Выполняется перед запуском тестов
        /// </summary>
        public TestFixture_InMemory()
        {
            RebuildProvider();
        }

        public void RebuildProvider()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            ServiceCollection = ServicesConfiguration.GetServiceCollectionForTests(configuration);
            var serviceProvider = GetServiceProvider();
            ServiceProvider = serviceProvider;
        }

        private IServiceProvider GetServiceProvider()
        {
            var serviceProvider = ServiceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();

            var dbContext = serviceProvider.GetService<DataContext>();
            var Initializer = new EfDbInitializer(dbContext);
            Initializer.InitializeDb();

            if (dbContext.Employees.Count() == 0)
            {
                throw new Exception("DB Initialize failed, check " + nameof(TestFixture_InMemory));
            }

            return serviceProvider;
        }

        public void Dispose()
        {
        }
    }
}
