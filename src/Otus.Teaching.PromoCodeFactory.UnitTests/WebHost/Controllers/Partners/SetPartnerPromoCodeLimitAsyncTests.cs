﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestFixture_InMemory>, IDisposable
    {
        
        private readonly PartnersController _partnersController;
        private DataContext _dataContext;
        private TestFixture_InMemory _testFixture;

        // Использование Moq не понадобилось, так как ничено не пришлось мокать (кроме
        // самой БД, которая для тестов сконфигурирована в режиме InMemory,
        // см. как - в ServicesConfiguration.ConfigureInMemoryContext)

        public SetPartnerPromoCodeLimitAsyncTests(TestFixture_InMemory testFixture)
        {
            var repo = testFixture.ServiceProvider.GetService<IRepository<Partner>>();
            _dataContext = testFixture.ServiceProvider.GetService<DataContext>();

            // Для создания экземпляра PartnersController не пришлось использовать AutoFixture.
            // Даже если в будущем появятся новые зависимости у PartnersController, можно будет
            // легко их получить через testFixture.ServiceProvider.GetService<T>, либо тут
            // на месте создать мок для них и передать в конструктор
            _partnersController = new PartnersController(repo);
            _testFixture = testFixture;
        }


        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_NotFoundParnter_ShouldFail()
        {
            // Arrange
            Guid unExisting = new Guid();
            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.UtcNow
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(unExisting,
                                                                                        setPartnerPromoCodeLimitRequest);
            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_BlockedPartner_ShouldFail()
        {
            // Arrange
            var nonActivePartner = FakeDataFactory.Partners.Where(a => a.IsActive == false).First();
            Guid guid1 = nonActivePartner.Id;
            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.UtcNow
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(guid1,
                setPartnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_ExistingLimit_ShouldAssignZero()
        {
            // Arrange
            var activePartner = FakeDataFactory.Partners.Where(a => a.IsActive == true && 
                                                               a.PartnerLimits.Where(p => p.CancelDate == null).Any()
                                                               ).First();
            Guid guid1 = activePartner.Id;

            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.UtcNow
            };
            var foundPartner = _dataContext.Partners
                .Where(a => a.Id == activePartner.Id).First();

            foundPartner.NumberIssuedPromoCodes = int.MaxValue;
            _dataContext.SaveChanges();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(guid1,
                setPartnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            foundPartner = _dataContext.Partners
                .Where(a => a.Id == activePartner.Id).First();

            foundPartner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_ExistingLimit_ShouldBeCalnceled()
        {
            // Arrange
            var activePartner = FakeDataFactory.Partners.Where(a => a.IsActive == true).First();
            Guid guid1 = activePartner.Id;

            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.UtcNow
            };
            var foundPartner = _dataContext.Partners
                .Where(a => a.Id == activePartner.Id).First();

            foundPartner.NumberIssuedPromoCodes = int.MaxValue;
            _dataContext.SaveChanges();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(guid1,
                setPartnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            foundPartner = _dataContext.Partners
                                .Where(a => a.Id == activePartner.Id).First();

            var foundCanceledLimits = _dataContext.PartnerPromoCodeLimits
                .Where(a => a.PartnerId == foundPartner.Id
                            && a.CancelDate != null)
                .ToList();

            foundCanceledLimits.Should().HaveCount(1);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_ZeroLimitTry_ShouldFail()
        {
            // Arrange
            var activePartner = FakeDataFactory.Partners.Where(a => a.IsActive == true).First();
            Guid guid1 = activePartner.Id;
            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 0,
                EndDate = DateTime.UtcNow
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(guid1,
                setPartnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_NewLimitAfterCreation_ShouldExistInDB()
        {
            // Arrange
            var activePartner = FakeDataFactory.Partners.Where(a => a.IsActive == true).First();
            Guid guid1 = activePartner.Id;
            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.UtcNow
            };

            var beforeCount = _dataContext.PartnerPromoCodeLimits.Count();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(guid1,
                setPartnerPromoCodeLimitRequest);

            // Assert
            var afterCount = _dataContext.PartnerPromoCodeLimits.Count();

            result.Should().BeAssignableTo<CreatedAtActionResult>();
            afterCount.Should().Be(beforeCount + 1);
        }

        public void Dispose()
        {
            _testFixture.RebuildProvider();
        }
    }
}